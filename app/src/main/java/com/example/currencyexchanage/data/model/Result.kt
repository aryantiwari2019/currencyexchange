package com.example.currencyexchanage.data.model

data class Result(
    val INR: Double,
    val rate: Double
)