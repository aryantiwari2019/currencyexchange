package com.example.currencyexchanage.data.model

data class ConvertCurrencyResponse(
    val amount: Int,
    val base: String,
    val ms: Int,
    val result: Result
)