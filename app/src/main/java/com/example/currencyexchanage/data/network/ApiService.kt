package com.example.currencyexchanage.data.network

import com.example.currencyexchanage.data.model.ConvertCurrencyResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("convert")
    suspend fun convertCurrency(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount:Double,
        @Query("api_key") api_key:String,): Response<ConvertCurrencyResponse>

}