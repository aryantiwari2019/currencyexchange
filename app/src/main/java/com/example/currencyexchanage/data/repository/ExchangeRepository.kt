package com.example.currencyexchanage.data.repository

import com.example.currencyexchanage.data.model.ConvertCurrencyResponse
import com.example.currencyexchanage.data.network.ApiService
import com.example.currencyexchanage.data.network.SafeApiRequest
import javax.inject.Inject

class ExchangeRepository @Inject constructor(private val apiService: ApiService): SafeApiRequest() {

    suspend fun convertCurrency(from: String, to: String,amount:Double,apikey:String): ConvertCurrencyResponse {
        return apiRequest {
            apiService.convertCurrency(from, to,amount, apikey)
        }
    }
}