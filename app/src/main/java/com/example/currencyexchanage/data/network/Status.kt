package com.example.currencyexchanage.data.network

enum class Status {
     Success,
     Loading,
     Failure
}
