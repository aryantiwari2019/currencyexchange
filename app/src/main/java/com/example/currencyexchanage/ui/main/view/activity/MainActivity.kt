package com.example.currencyexchanage.ui.main.view.activity

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.example.currencyexchanage.R
import com.example.currencyexchanage.data.network.Status
import com.example.currencyexchanage.ui.base.BaseActivity
import com.example.currencyexchanage.ui.main.viewmodel.CurrencyViewModel
import com.example.currencyexchanage.utils.HelperMethods
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private val viewModel by viewModels<CurrencyViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listener()
    }

    private fun listener(){
        tospinner.setSelection(59)
        convert!!.setOnClickListener {
            when {
                amount!!.text.isEmpty() -> {
                    alert!!.showError(getString(R.string.amount_should_not_be_blank))
                }
                amount!!.text!!.toString().toDouble() <= 0 -> {
                    alert!!.showError(getString(R.string.amount_error))

                }
                else -> convertCurrency()
            }
        }
    }

    private fun convertCurrency(){
        viewModel.convertCurrency(from?.selectedItem.toString(),tospinner?.selectedItem.toString(),amount.text.toString()).observe(this,
            {
                it?.let {
                        resource ->
                    when(resource.status){
                        Status.Success->{
                            it.data?.let {
                                    it1 ->
                                    resultView.visibility=View.VISIBLE
                                    (tospinner?.selectedItem.toString()+" "+ HelperMethods.roundTwoDecimals(it1.result.rate* amount.text.toString()
                                    .toDouble())).also { result.text = it }

                            }
                            progress?.dismissSweet()

                        }
                        Status.Failure->{
                            progress?.dismissSweet()
                        }
                        Status.Loading->{
                            progress?.showSweetDialog()
                        }
                    }
                }
            })

    }
}