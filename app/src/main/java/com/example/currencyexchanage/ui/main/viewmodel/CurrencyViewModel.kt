package com.example.currencyexchanage.ui.main.viewmodel


import androidx.lifecycle.liveData
import com.example.currencyexchanage.data.network.Resource
import com.example.currencyexchanage.data.repository.ExchangeRepository
import com.example.currencyexchanage.ui.base.BaseViewModel
import com.example.currencyexchanage.utils.MyKeys
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class CurrencyViewModel @Inject constructor(private val repository: ExchangeRepository):BaseViewModel() {

    fun convertCurrency(
        from: String?,
        to: String?,
        amount: String,)  = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.convertCurrency(from!!,to!!,amount.toDouble(),MyKeys.ExchangeKey)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

}

