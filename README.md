Description:-
the main purpose of the application is to convert from one currency to another,
user need to input  currency information and amount and then press go,
app will calculate the result and display.

App language:-
Kotlin

Architecture pattern:-
MVVM.


Android version:-
up to api level 31.


The app has following packages:-
data: It contains all the data accessing and manipulating components.
ui: View classes along with their corresponding ViewModel.
di: dependency injection for dependent classes and interfaces.
utils: Utility classes

Library and Api:-
Hilt jetpack component for dependency injection.
ViewModel persistence library
livedata for lifecycle awareness
co-routines light weighted thread
view-binding library to bind view.
retrofit for network operation
gson for conversion


API:-
i have used currency exchange api of fastFOREX.
https://api.fastforex.io/convert?from=USD&to=EUR&amount=1.00


